import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Car } from './../models/car.model';
import * as CarActions from './../actions/car.action';
import { AppState } from './../app.state';

@Component({
  selector: 'read-car',
  templateUrl: './read-car.component.html',
  styleUrls: ['./read-car.component.scss']
})
export class ReadCarComponent implements OnInit {

  cars: Observable<Car[]>;

  constructor(private store: Store<AppState>) {
    this.cars = store.select('car');
  }

  ngOnInit(): void {
  }

  // In the class, add:
  delCar(index: number) {
    this.store.dispatch(new CarActions.RemoveCar(index));
  }

}
