import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Other imports removed for brevity
import { StoreModule } from '@ngrx/store';
import { TutorialReducer } from './reducers/tutorial.reducer';
import { CarReducer } from './reducers/car.reducer';
import { ReadComponent } from './read/read.component';
import { CreateComponent } from './create/create.component';
import { AddCarComponent } from './add-car/add-car.component';
import { ReadCarComponent } from './read-car/read-car.component';

import { NgxDocViewerModule } from "ngx-doc-viewer";

@NgModule({
  declarations: [
    AppComponent,
    ReadComponent,
    CreateComponent,
    AddCarComponent,
    ReadCarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({
      tutorial: TutorialReducer,
      car: CarReducer
    }),
    NgxDocViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
