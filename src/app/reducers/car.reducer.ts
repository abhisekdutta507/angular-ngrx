import { reject } from 'lodash';
import { Car } from './../models/car.model';
import * as CarActions from './../actions/car.action';

// Section 1
const initialState: Car = {
  name: 'BMW X1'
};

// Section 2
export function CarReducer(state: Car[] = [initialState], action: CarActions.Actions) {

  // Section 3
  switch (action.type) {
    case CarActions.ADD_CAR:
      return [...state, action.payload];
    case CarActions.REMOVE_CAR:
      const newState = reject(state, (tut: Car, i: number) => i === action.payload);
      return newState;
    default:
      return state;
  }
}