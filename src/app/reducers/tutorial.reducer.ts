import { reject } from 'lodash';
import { Tutorial } from './../models/tutorial.model';
import * as TutorialActions from './../actions/tutorial.action';

// Section 1
const initialState: Tutorial = {
  name: 'Initial Tutorial',
  url: 'http://google.com'
};

// Section 2
export function TutorialReducer(state: Tutorial[] = [initialState], action: TutorialActions.Actions) {

  // Section 3
  switch (action.type) {
    case TutorialActions.ADD_TUTORIAL:
      return [...state, action.payload];
    case TutorialActions.REMOVE_TUTORIAL:
      const newState = reject(state, (tut: Tutorial, i: number) => i === action.payload);
      return newState;
    default:
      return state;
  }
}