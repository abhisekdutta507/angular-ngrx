import { Tutorial } from './models/tutorial.model';
import { Car } from './models/car.model';

export interface AppState {
  readonly tutorial: Tutorial[];
  readonly car: Car[];
}