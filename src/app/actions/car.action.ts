// Section 1
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Car } from './../models/car.model';

// Section 2
export const ADD_CAR     = '[CAR] Add';
export const REMOVE_CAR  = '[CAR] Remove';

// Section 3
export class AddCar implements Action {
  readonly type = ADD_CAR;

  constructor(public payload: Car) { }
}

export class RemoveCar implements Action {
  readonly type = REMOVE_CAR;

  constructor(public payload: number) { }
}

// Section 4
export type Actions = AddCar | RemoveCar;